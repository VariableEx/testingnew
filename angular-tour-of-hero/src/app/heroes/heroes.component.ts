import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
// aur jo humne list banayi h heros ki ushko hume yaha import kiya h web p display karne k liye
import { HEROES } from '../mock-heroes';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  // jab humein hero ki property add karni ho to uska ya fir object banana hota h ya koi variable   """hero = 'Windstorm';"""
  // hero = 'Windstorm';

  // humne pahle ki hero k string ko hata kar ushi naam ka ek object banaya h aur yaha par hum ushe value assign kar rahe h
  
  hero: Hero = {
    id: 1,
    name: 'Windstorm'
  }

  // jo hero ki list humne import ki h usko use karne k liye uska ek object banana zaroori h jiske liye humne ye line likhi h
  heroes = HEROES;



  constructor() { }

  ngOnInit() {
  }

  selectedHero: Hero;
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

}