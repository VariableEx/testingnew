import { Component, EventEmitter, Output } from '@angular/core';
import { Post } from '../post.model';
import { NgForm } from '@angular/forms';


@Component({
  selector : 'app-post-create',
  templateUrl : './post-create.component.html',
  styleUrls : ['./post-create.component.css']
})
export class PostCreateComponent{

  // enteredValue = '';
  enteredContent = '';
  enteredTitle = '';
  @Output() postCreated = new EventEmitter<Post>();
  // newPost = '';

  onAppPost(form: NgForm) {
    // console.dir(emailInput,passInput);
    // this.newPost = emailInput.value;
    // this.newPost = this.enteredValue;
    const post: Post = {
      title : form.value.title,
      content : form.value.content
    };
    this.postCreated.emit(post);
  }
}
