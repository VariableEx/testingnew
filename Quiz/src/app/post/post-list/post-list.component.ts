import { Component, Input } from "@angular/core";
import { Post } from '../post.model';

@Component ( {
  selector : 'app-post-list',
  templateUrl : './post-list.component.html',
  styleUrls : ['./post-list.component.css']
})

export class PostListComponent{
  // posts = [
  //   {title : 'first post', content: 'this is post 1'},
  //   {title : 'second post', content: 'this is post 2'},
  //   {title : 'third post', content: 'this is post 3'},
  //   {title : 'fourth post', content: 'this is post 4'}
  // ];
  @Input() posts:Post [] = [];
}
